class GamsResultProcessor:
    def __init__(
        self,
        graph,
        gdx_file,
        sql_output_file,
    ):
        self.graph = graph.copy()
        self.gdx2sql = '/media/muhammadali/local_disk_2/Linux/Softwares/gams25.1_linux_x64_64_sfx/gdx2sqlite'
        self.sql_output_file = sql_output_file
        self.make_sql_file(gdx_file)

    def get_new_graph(self):
        data = self.read_sql_file()
        self.add_data_to_graph(self.graph, data)
        return self.graph


    def add_data_to_graph(self, graph, data):
        alphas = data['alphas']
        for i, v in alphas['u_V'].items():
            graph.vs[int(v)]['isAttacked'] = True if alphas['level'][i] == 1 else 0

        betas = data['betas']
        for i, v in betas['u_V'].items():
            graph.vs[int(v)]['isEnabled'] = True if betas['level'][i] == 1 else 0

    def read_sql_file(self):
        from sqlalchemy import create_engine, inspect
        import pandas as pd
        engine = create_engine(f'sqlite:////{self.sql_output_file}')
        # inspector = inspect(engine)
        # print(inspector.get_table_names())
        data = {
            'alphas' : pd.read_sql_table('alpha', engine).to_dict(),
            'betas' : pd.read_sql_table('beta', engine).to_dict(),
            'scalars' : pd.read_sql_table('scalarvariables', engine).to_dict(),
        }
        return data

    def make_sql_file(self, gdx_file):
        import subprocess
        p = subprocess.Popen(
            f'{self.gdx2sql} -i {gdx_file} -o {self.sql_output_file}',
            shell=True
        )
        retval = p.wait()


