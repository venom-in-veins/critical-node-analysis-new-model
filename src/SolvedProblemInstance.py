from GamsInputReader import GamsInputReader
from GamsOutputReader import GamsOutputReader
from GraphGenerator import GraphGenerator
import igraph as iGraph

from common import (
    solution_graph_cache_dir_path_for,
    solution_graph_filename_for,
)
import os

class SolvedProblemInstance:
    def __init__(self, solution_graph):
        self.solution_graph = solution_graph
        self._make_original_graph()

    def _make_original_graph(self):
        self.original_graph = GraphGenerator.generate_original_for(self.solution_graph)

    @staticmethod
    def create(target_folder, filename):
        cache_dir = solution_graph_cache_dir_path_for(target_folder)
        graph_filename = os.path.join(
                cache_dir,
                solution_graph_filename_for(filename),
        )
        if os.path.isfile(graph_filename):
            solution_graph = iGraph.Graph.Read_Picklez(graph_filename)
        else:
            if not os.path.exists(cache_dir):
                os.makedirs(cache_dir)
            input_reader = GamsInputReader(
                os.path.join(target_folder, f'{filename}.gms'),
            )
            output_reader = GamsOutputReader(
                input_reader.graph,
                os.path.join(target_folder, f'{filename}.gdx'),
                os.path.join(target_folder, f'{filename}.sql'),
            )
            solution_graph = output_reader.get_new_graph()
            solution_graph.write_picklez(graph_filename)
        return SolvedProblemInstance(solution_graph)

