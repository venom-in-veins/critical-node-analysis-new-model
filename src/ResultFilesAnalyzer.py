from ResultFileAnalyzer import ResultFileAnalyzer
from SolvedProblemInstance import SolvedProblemInstance
from common import (
    read_info_file,
    solution_graph_filename_for,
)
import os, json

class ResultFilesAnalyzer:
    def __init__(self, target_folder, info_filename):
        self.target_info = target_folder
        self.info = read_info_file(target_folder, info_filename)
        self.setup(target_folder)

    def setup(self, target_folder):
        self.analyzers = [
            ResultFileAnalyzer(
                v,
                SolvedProblemInstance.create(target_folder, v['filename']),
            )
            for v in self.info
        ]

    def draw(self):
        for a in self.analyzers:
            a.draw()

    def run(self):
        for a in self.analyzers:
            a.run()

    def save(self):
        self.write_info(self.target_info)

    def write_info(self, target_folder):
        info = [
            a.info
            for a in self.analyzers
        ]
        with open(os.path.join(target_folder, 'new_info.json'), 'w') as file:
            file.write(json.dumps(info))