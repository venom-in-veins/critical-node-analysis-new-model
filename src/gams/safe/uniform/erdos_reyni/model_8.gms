* model_8.gms

* ***input_data***
set u_V vertices in the graph
/ 0 * 119 /;

alias(u_V, v_V);
* ***node_count***
* 120
* ***node_count***
set e_E(u_V, v_V)
/
* ***edges***
	0.28
	0.82
	1.50
	2.56
	3.83
	4.8
	5.18
	5.73
	5.112
	7.24
	7.84
	8.51
	8.58
	8.71
	9.84
	9.114
	10.64
	11.17
	11.57
	12.85
	12.86
	13.76
	13.94
	14.100
	14.116
	15.77
	15.108
	16.47
	17.46
	18.39
	18.44
	18.75
	19.30
	20.87
	20.112
	21.94
	22.30
	22.57
	22.63
	22.102
	23.9
	23.24
	23.71
	23.82
	24.50
	24.72
	24.91
	25.38
	25.75
	25.80
	25.81
	25.89
	25.107
	26.111
	26.116
	27.56
	27.81
	27.86
	28.15
	29.28
	30.9
	30.85
	32.57
	33.64
	33.65
	33.112
	34.87
	34.101
	34.112
	35.12
	35.52
	35.87
	35.113
	36.23
	36.59
	38.33
	38.98
	38.100
	39.29
	40.73
	41.16
	41.36
	41.47
	41.90
	41.100
	42.19
	42.28
	42.36
	42.98
	43.50
	44.41
	45.17
	45.29
	46.0
	46.116
	47.2
	47.49
	48.15
	48.39
	48.74
	48.100
	48.103
	49.8
	49.31
	49.80
	50.75
	51.70
	51.78
	52.81
	53.33
	53.59
	54.94
	54.111
	55.63
	57.87
	58.73
	59.2
	59.34
	59.95
	60.2
	60.35
	61.31
	61.54
	62.72
	63.54
	63.89
	64.79
	64.101
	65.32
	65.103
	66.4
	66.61
	67.119
	67.108
	68.41
	69.61
	69.92
	70.1
	70.31
	70.32
	71.36
	71.116
	72.103
	73.47
	73.65
	73.87
	74.8
	75.43
	75.61
	75.72
	75.74
	76.3
	76.77
	77.59
	77.76
	77.96
	77.99
	77.102
	79.19
	79.26
	79.34
	79.52
	80.6
	80.18
	81.7
	81.44
	81.83
	81.89
	81.94
	81.105
	82.47
	82.51
	82.105
	83.22
	83.25
	84.101
	86.50
	86.67
	87.47
	87.82
	87.111
	89.5
	89.64
	90.53
	90.111
	91.23
	92.72
	93.13
	93.18
	93.102
	94.10
	94.46
	95.26
	95.119
	96.34
	96.87
	96.119
	97.3
	97.32
	97.54
	97.69
	99.62
	99.67
	101.11
	101.44
	101.82
	102.112
	103.63
	103.78
	104.56
	105.36
	105.37
	105.49
	105.65
	105.91
	105.93
	105.116
	107.7
	107.57
	107.73
	108.119
	109.6
	109.22
	109.70
	110.19
	110.60
	111.91
	111.101
	112.15
	112.31
	112.49
	112.78
	112.106
	112.115
	113.75
	114.31
	116.35
	116.88
	117.72
	118.107
* ***edges***
/;

parameters
	B /
* ***attack_budget***
1
* ***attack_budget***
/
	D /
* ***target_degradation***
1
* ***target_degradation***
/
	weight(u_V)
	/
* ***node_weights***
		0 0.04
		1 0.42
		2 0.34
		3 0.11
		4 0.25
		5 0.71
		6 0.42
		7 0.89
		8 0.58
		9 0.39
		10 0.39
		11 0.01
		12 0.1
		13 0.1
		14 1.0
		15 0.93
		16 0.89
		17 0.54
		18 0.46
		19 0.35
		20 0.33
		21 0.82
		22 0.98
		23 0.06
		24 0.22
		25 0.92
		26 0.21
		27 0.32
		28 0.75
		29 0.69
		30 0.26
		31 0.29
		32 0.78
		33 0.64
		34 0.78
		35 0.49
		36 0.57
		37 0.98
		38 0.82
		39 0.72
		40 0.55
		41 0.15
		42 0.99
		43 0.91
		44 0.78
		45 0.46
		46 0.74
		47 0.06
		48 0.06
		49 0.27
		50 0.25
		51 0.38
		52 0.68
		53 0.87
		54 0.98
		55 0.04
		56 0.44
		57 0.88
		58 0.43
		59 0.67
		60 0.16
		61 0.31
		62 0.09
		63 0.44
		64 0.24
		65 0.63
		66 0.63
		67 0.57
		68 0.77
		69 0.13
		70 0.78
		71 0.51
		72 0.93
		73 0.4
		74 0.51
		75 0.46
		76 0.3
		77 0.71
		78 0.57
		79 0.2
		80 0.15
		81 0.41
		82 0.94
		83 0.13
		84 0.52
		85 0.79
		86 0.26
		87 0.82
		88 0.49
		89 0.22
		90 0.9
		91 0.62
		92 0.01
		93 0.43
		94 0.74
		95 0.59
		96 0.84
		97 0.44
		98 0.67
		99 0.97
		100 0.98
		101 0.88
		102 0.77
		103 0.97
		104 0.23
		105 0.87
		106 0.53
		107 0.34
		108 0.02
		109 0.28
		110 0.86
		111 0.24
		112 0.57
		113 0.42
		114 0.01
		115 0.11
		116 0.65
		117 0.71
		118 0.14
		119 0.45
* ***node_weights***
	/;

* ***input_data***
binary variables
    alpha(u_V) attacked
    beta(u_V) enabled
    y(u_V) supply is on
    ;

variables
    total_disabled_nodes
    attack_cost
    ;

equations
    objective_function_a
    objective_function_b
    e11(u_V)
    e12(u_V)
    e2(u_V)
    e3(u_V)
    e4(u_V)
    e5(u_V)
    e6_a
    e6_b
    ;

objective_function_a .. total_disabled_nodes =e=
    sum(u_V, 1 - beta(u_V));

objective_function_b .. attack_cost =e=
    sum(u_V, weight(u_V) * alpha(u_V));


$macro deg_in(u_V) sum(v_V $ e_E(v_V, u_V), 1)

e11(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= 1 - alpha(u_V);
e12(u_V) $ (deg_in(u_V) = 0) .. beta(u_V) =e= 1 - alpha(u_V);
e2(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= y(u_V);
e3(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =g= y(u_V) - alpha(u_V);
e4(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =g= 0;
e5(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =l= deg_in(u_V) - 1;
e6_a .. sum(u_V, weight(u_V) * alpha(u_V)) =l= B;
e6_b .. sum(u_V, 1 - beta(u_V)) / sum(u_V, 1) =g= D;


model damage_maximizer
    /
    objective_function_a,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_a
    /;

model attackCost_minimizer
    /
    objective_function_b,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_b
    /;

* solve damage_maximizer using mip maximizing total_disabled_nodes;
solve attackCost_minimizer using mip minimizing attack_cost;
