* model_1.gms

* ***input_data***
set u_V vertices in the graph
/ 0 * 59 /;

alias(u_V, v_V);
* ***node_count***
* 60
* ***node_count***
set e_E(u_V, v_V)
/
* ***edges***
	0.3
	0.23
	0.47
	1.34
	1.49
	1.51
	2.9
	2.16
	3.24
	3.27
	3.38
	3.54
	4.9
	6.3
	7.27
	7.35
	7.58
	8.12
	8.41
	9.24
	9.32
	10.18
	10.30
	10.50
	11.2
	11.3
	11.8
	11.10
	12.6
	14.26
	14.41
	15.6
	15.35
	16.12
	16.27
	16.45
	16.56
	17.21
	17.51
	18.31
	19.15
	19.27
	19.40
	20.53
	21.5
	21.31
	21.57
	22.5
	23.28
	23.53
	25.13
	25.47
	26.42
	26.51
	26.58
	27.8
	28.3
	28.9
	28.33
	29.8
	29.20
	29.46
	30.37
	30.38
	30.48
	31.7
	31.12
	32.35
	32.38
	33.3
	33.16
	33.27
	33.59
	34.19
	35.28
	36.7
	36.10
	36.29
	36.33
	36.57
	37.3
	37.48
	39.41
	40.12
	40.15
	41.30
	42.29
	42.31
	43.22
	43.37
	44.16
	45.18
	45.52
	46.28
	47.23
	47.30
	48.1
	48.17
	48.53
	51.28
	52.0
	52.17
	52.25
	52.33
	52.56
	53.36
	53.40
	54.43
	54.45
	54.53
	55.43
	57.27
	58.8
	58.20
	58.33
	58.51
	59.3
	59.10
	59.27
	59.47
* ***edges***
/;

parameters
	B /
* ***attack_budget***
1
* ***attack_budget***
/
	D /
* ***target_degradation***
1
* ***target_degradation***
/
	weight(u_V)
	/
* ***node_weights***
		0 0.02
		1 0.75
		2 0.18
		3 0.97
		4 0.27
		5 0.41
		6 0.72
		7 0.58
		8 0.3
		9 0.23
		10 0.06
		11 0.61
		12 0.09
		13 0.67
		14 0.84
		15 0.82
		16 0.63
		17 0.66
		18 0.52
		19 0.5
		20 0.89
		21 0.32
		22 0.98
		23 0.78
		24 0.12
		25 0.42
		26 0.24
		27 0.29
		28 0.08
		29 0.9
		30 0.36
		31 0.75
		32 0.03
		33 0.24
		34 0.96
		35 0.44
		36 0.29
		37 0.56
		38 0.49
		39 0.59
		40 0.28
		41 0.45
		42 0.62
		43 0.45
		44 0.9
		45 0.06
		46 0.12
		47 0.95
		48 0.82
		49 0.38
		50 0.67
		51 0.29
		52 0.72
		53 0.09
		54 0.11
		55 0.75
		56 0.43
		57 0.21
		58 0.62
		59 0.01
* ***node_weights***
	/;

* ***input_data***
binary variables
    alpha(u_V) attacked
    beta(u_V) enabled
    y(u_V) supply is on
    ;

variables
    total_disabled_nodes
    attack_cost
    ;

equations
    objective_function_a
    objective_function_b
    e11(u_V)
    e12(u_V)
    e2(u_V)
    e3(u_V)
    e4(u_V)
    e5(u_V)
    e6_a
    e6_b
    ;

objective_function_a .. total_disabled_nodes =e=
    sum(u_V, 1 - beta(u_V));

objective_function_b .. attack_cost =e=
    sum(u_V, weight(u_V) * alpha(u_V));


$macro deg_in(u_V) sum(v_V $ e_E(v_V, u_V), 1)

e11(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= 1 - alpha(u_V);
e12(u_V) $ (deg_in(u_V) = 0) .. beta(u_V) =e= 1 - alpha(u_V);
e2(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= y(u_V);
e3(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =g= y(u_V) - alpha(u_V);
e4(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =g= 0;
e5(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =l= deg_in(u_V) - 1;
e6_a .. sum(u_V, weight(u_V) * alpha(u_V)) =l= B;
e6_b .. sum(u_V, 1 - beta(u_V)) / sum(u_V, 1) =g= D;


model damage_maximizer
    /
    objective_function_a,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_a
    /;

model attackCost_minimizer
    /
    objective_function_b,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_b
    /;

* solve damage_maximizer using mip maximizing total_disabled_nodes;
solve attackCost_minimizer using mip minimizing attack_cost;
