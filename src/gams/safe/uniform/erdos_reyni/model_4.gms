* model_4.gms

* ***input_data***
set u_V vertices in the graph
/ 0 * 59 /;

alias(u_V, v_V);
* ***node_count***
* 60
* ***node_count***
set e_E(u_V, v_V)
/
* ***edges***
	0.25
	0.46
	1.30
	2.10
	2.19
	3.24
	4.46
	5.28
	5.39
	6.43
	6.52
	7.22
	7.43
	7.55
	8.7
	9.3
	9.45
	10.3
	10.17
	11.6
	11.48
	11.57
	12.30
	13.29
	14.2
	14.31
	15.7
	15.59
	16.14
	16.15
	17.55
	18.22
	18.55
	18.56
	19.2
	19.11
	19.34
	19.43
	20.48
	21.38
	22.9
	22.24
	22.32
	23.4
	23.7
	23.41
	24.16
	26.4
	26.14
	26.56
	26.57
	26.58
	27.4
	30.1
	30.6
	30.50
	31.1
	32.10
	32.14
	34.8
	34.57
	35.18
	35.21
	35.31
	35.36
	35.42
	35.57
	36.10
	36.16
	36.19
	36.30
	36.47
	37.24
	37.52
	38.1
	38.22
	38.24
	38.31
	39.41
	40.35
	42.7
	43.6
	43.11
	44.18
	44.47
	45.6
	45.7
	45.36
	45.53
	46.14
	46.56
	47.7
	47.15
	47.25
	47.26
	48.3
	48.9
	48.31
	48.33
	48.38
	48.49
	49.8
	49.36
	49.40
	50.43
	51.37
	51.39
	51.41
	52.23
	52.47
	53.8
	53.26
	55.43
	56.35
	56.53
	57.33
	58.9
	58.54
	59.26
	59.48
* ***edges***
/;

parameters
	B /
* ***attack_budget***
1
* ***attack_budget***
/
	D /
* ***target_degradation***
1
* ***target_degradation***
/
	weight(u_V)
	/
* ***node_weights***
		0 0.21
		1 0.15
		2 0.33
		3 0.79
		4 0.89
		5 0.46
		6 0.67
		7 0.25
		8 0.52
		9 0.8
		10 0.23
		11 0.42
		12 0.95
		13 0.24
		14 0.06
		15 0.73
		16 0.42
		17 0.22
		18 0.35
		19 0.98
		20 0.57
		21 0.7
		22 0.67
		23 0.47
		24 0.73
		25 0.27
		26 0.69
		27 0.63
		28 0.64
		29 0.85
		30 0.42
		31 0.0
		32 0.23
		33 0.52
		34 0.41
		35 0.13
		36 0.71
		37 0.92
		38 0.25
		39 0.42
		40 0.75
		41 0.87
		42 0.04
		43 0.37
		44 0.44
		45 0.11
		46 0.07
		47 0.44
		48 0.1
		49 0.36
		50 0.32
		51 0.29
		52 0.86
		53 0.5
		54 0.39
		55 0.39
		56 0.14
		57 0.5
		58 0.75
		59 0.28
* ***node_weights***
	/;

* ***input_data***
binary variables
    alpha(u_V) attacked
    beta(u_V) enabled
    y(u_V) supply is on
    ;

variables
    total_disabled_nodes
    attack_cost
    ;

equations
    objective_function_a
    objective_function_b
    e11(u_V)
    e12(u_V)
    e2(u_V)
    e3(u_V)
    e4(u_V)
    e5(u_V)
    e6_a
    e6_b
    ;

objective_function_a .. total_disabled_nodes =e=
    sum(u_V, 1 - beta(u_V));

objective_function_b .. attack_cost =e=
    sum(u_V, weight(u_V) * alpha(u_V));


$macro deg_in(u_V) sum(v_V $ e_E(v_V, u_V), 1)

e11(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= 1 - alpha(u_V);
e12(u_V) $ (deg_in(u_V) = 0) .. beta(u_V) =e= 1 - alpha(u_V);
e2(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= y(u_V);
e3(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =g= y(u_V) - alpha(u_V);
e4(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =g= 0;
e5(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =l= deg_in(u_V) - 1;
e6_a .. sum(u_V, weight(u_V) * alpha(u_V)) =l= B;
e6_b .. sum(u_V, 1 - beta(u_V)) / sum(u_V, 1) =g= D;


model damage_maximizer
    /
    objective_function_a,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_a
    /;

model attackCost_minimizer
    /
    objective_function_b,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_b
    /;

* solve damage_maximizer using mip maximizing total_disabled_nodes;
solve attackCost_minimizer using mip minimizing attack_cost;
