* model_5.gms

* ***input_data***
set u_V vertices in the graph
/ 0 * 59 /;

alias(u_V, v_V);
* ***node_count***
* 60
* ***node_count***
set e_E(u_V, v_V)
/
* ***edges***
	0.6
	0.10
	0.20
	0.24
	1.31
	1.32
	1.35
	2.25
	2.29
	3.15
	5.33
	5.36
	5.42
	7.10
	7.39
	7.45
	8.46
	9.52
	10.31
	10.39
	11.28
	12.16
	12.26
	12.57
	13.31
	13.40
	13.50
	13.57
	14.5
	14.19
	14.56
	15.13
	16.0
	16.21
	17.2
	17.32
	18.0
	18.44
	20.31
	21.19
	21.22
	21.46
	22.14
	23.13
	23.33
	25.20
	25.32
	25.33
	27.11
	27.37
	28.4
	28.30
	29.30
	29.56
	30.4
	31.11
	31.17
	31.35
	32.15
	32.51
	33.19
	33.56
	34.21
	34.40
	35.24
	35.28
	35.33
	35.42
	35.46
	36.26
	38.1
	38.28
	38.39
	38.49
	39.12
	39.23
	39.48
	40.4
	40.5
	40.7
	41.22
	42.15
	42.32
	43.36
	43.49
	44.13
	45.28
	45.37
	45.48
	46.0
	46.10
	46.48
	47.54
	48.2
	48.33
	48.36
	48.45
	49.41
	49.47
	50.1
	50.6
	50.52
	50.54
	51.12
	51.33
	53.22
	53.49
	53.55
	54.47
	54.51
	55.32
	55.33
	56.16
	56.28
	56.43
	56.47
	58.34
	58.43
	58.49
	59.8
* ***edges***
/;

parameters
	B /
* ***attack_budget***
1
* ***attack_budget***
/
	D /
* ***target_degradation***
1
* ***target_degradation***
/
	weight(u_V)
	/
* ***node_weights***
		0 0.88
		1 0.07
		2 0.05
		3 0.44
		4 0.46
		5 0.43
		6 0.29
		7 0.01
		8 0.65
		9 0.89
		10 0.29
		11 0.93
		12 0.56
		13 0.22
		14 0.7
		15 0.72
		16 0.45
		17 0.33
		18 0.59
		19 0.71
		20 0.49
		21 0.55
		22 0.68
		23 0.27
		24 0.33
		25 0.67
		26 0.48
		27 0.92
		28 0.42
		29 0.35
		30 0.41
		31 0.1
		32 0.7
		33 0.74
		34 0.09
		35 0.87
		36 0.94
		37 0.93
		38 0.15
		39 0.3
		40 0.32
		41 0.02
		42 0.73
		43 0.22
		44 0.69
		45 0.88
		46 0.08
		47 0.77
		48 0.63
		49 0.4
		50 0.49
		51 0.55
		52 0.76
		53 0.77
		54 0.42
		55 0.28
		56 0.1
		57 0.27
		58 0.95
		59 0.49
* ***node_weights***
	/;

* ***input_data***
binary variables
    alpha(u_V) attacked
    beta(u_V) enabled
    y(u_V) supply is on
    ;

variables
    total_disabled_nodes
    attack_cost
    ;

equations
    objective_function_a
    objective_function_b
    e11(u_V)
    e12(u_V)
    e2(u_V)
    e3(u_V)
    e4(u_V)
    e5(u_V)
    e6_a
    e6_b
    ;

objective_function_a .. total_disabled_nodes =e=
    sum(u_V, 1 - beta(u_V));

objective_function_b .. attack_cost =e=
    sum(u_V, weight(u_V) * alpha(u_V));


$macro deg_in(u_V) sum(v_V $ e_E(v_V, u_V), 1)

e11(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= 1 - alpha(u_V);
e12(u_V) $ (deg_in(u_V) = 0) .. beta(u_V) =e= 1 - alpha(u_V);
e2(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= y(u_V);
e3(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =g= y(u_V) - alpha(u_V);
e4(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =g= 0;
e5(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =l= deg_in(u_V) - 1;
e6_a .. sum(u_V, weight(u_V) * alpha(u_V)) =l= B;
e6_b .. sum(u_V, 1 - beta(u_V)) / sum(u_V, 1) =g= D;


model damage_maximizer
    /
    objective_function_a,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_a
    /;

model attackCost_minimizer
    /
    objective_function_b,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_b
    /;

* solve damage_maximizer using mip maximizing total_disabled_nodes;
solve attackCost_minimizer using mip minimizing attack_cost;
