* model_2.gms

* ***input_data***
set u_V vertices in the graph
/ 0 * 59 /;

alias(u_V, v_V);
* ***node_count***
* 60
* ***node_count***
set e_E(u_V, v_V)
/
* ***edges***
	2.17
	3.12
	3.20
	3.21
	4.33
	5.44
	5.53
	6.30
	6.38
	7.3
	7.34
	7.45
	7.57
	8.33
	8.34
	8.36
	9.29
	9.38
	11.4
	11.9
	11.10
	12.5
	12.45
	13.10
	13.22
	14.6
	14.52
	14.57
	16.1
	16.53
	17.36
	17.46
	20.17
	20.59
	20.51
	22.6
	23.2
	24.48
	25.18
	26.40
	27.20
	27.23
	27.48
	28.9
	28.11
	28.47
	29.44
	29.56
	30.27
	31.18
	31.43
	31.55
	32.9
	32.51
	33.31
	33.43
	34.14
	34.17
	34.28
	34.54
	35.6
	35.8
	35.34
	35.48
	36.49
	37.16
	37.27
	37.39
	38.59
	38.43
	39.51
	39.56
	40.1
	40.9
	40.12
	40.26
	40.42
	40.45
	41.24
	42.4
	43.7
	43.9
	44.6
	44.21
	44.22
	44.34
	44.59
	45.21
	45.26
	45.36
	46.14
	46.17
	47.20
	47.34
	47.37
	48.15
	48.31
	48.39
	48.58
	49.13
	49.22
	50.16
	50.33
	50.44
	50.47
	50.53
	51.32
	51.55
	53.12
	53.13
	53.34
	54.39
	55.23
	55.46
	57.6
	57.24
	57.31
	58.10
	58.33
	59.20
* ***edges***
/;

parameters
	B /
* ***attack_budget***
1
* ***attack_budget***
/
	D /
* ***target_degradation***
1
* ***target_degradation***
/
	weight(u_V)
	/
* ***node_weights***
		0 0.48
		1 0.04
		2 0.21
		3 0.22
		4 0.63
		5 0.48
		6 0.42
		7 0.51
		8 0.41
		9 0.5
		10 0.57
		11 0.69
		12 0.32
		13 0.95
		14 0.81
		15 0.57
		16 0.13
		17 0.9
		18 0.0
		19 0.93
		20 0.67
		21 0.68
		22 0.42
		23 0.01
		24 0.8
		25 0.24
		26 0.21
		27 0.12
		28 0.48
		29 0.59
		30 0.22
		31 0.03
		32 0.5
		33 0.44
		34 0.87
		35 0.29
		36 0.9
		37 0.89
		38 0.85
		39 0.15
		40 0.78
		41 0.26
		42 0.71
		43 0.51
		44 0.55
		45 0.64
		46 0.63
		47 0.01
		48 0.03
		49 0.67
		50 0.81
		51 0.28
		52 0.41
		53 0.56
		54 0.44
		55 0.22
		56 0.95
		57 0.37
		58 0.53
		59 0.8
* ***node_weights***
	/;

* ***input_data***
binary variables
    alpha(u_V) attacked
    beta(u_V) enabled
    y(u_V) supply is on
    ;

variables
    total_disabled_nodes
    attack_cost
    ;

equations
    objective_function_a
    objective_function_b
    e11(u_V)
    e12(u_V)
    e2(u_V)
    e3(u_V)
    e4(u_V)
    e5(u_V)
    e6_a
    e6_b
    ;

objective_function_a .. total_disabled_nodes =e=
    sum(u_V, 1 - beta(u_V));

objective_function_b .. attack_cost =e=
    sum(u_V, weight(u_V) * alpha(u_V));


$macro deg_in(u_V) sum(v_V $ e_E(v_V, u_V), 1)

e11(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= 1 - alpha(u_V);
e12(u_V) $ (deg_in(u_V) = 0) .. beta(u_V) =e= 1 - alpha(u_V);
e2(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= y(u_V);
e3(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =g= y(u_V) - alpha(u_V);
e4(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =g= 0;
e5(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =l= deg_in(u_V) - 1;
e6_a .. sum(u_V, weight(u_V) * alpha(u_V)) =l= B;
e6_b .. sum(u_V, 1 - beta(u_V)) / sum(u_V, 1) =g= D;


model damage_maximizer
    /
    objective_function_a,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_a
    /;

model attackCost_minimizer
    /
    objective_function_b,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_b
    /;

* solve damage_maximizer using mip maximizing total_disabled_nodes;
solve attackCost_minimizer using mip minimizing attack_cost;
