* model_3.gms

* ***input_data***
set u_V vertices in the graph
/ 0 * 59 /;

alias(u_V, v_V);
* ***node_count***
* 60
* ***node_count***
set e_E(u_V, v_V)
/
* ***edges***
	0.34
	0.47
	1.59
	2.56
	3.30
	3.40
	4.1
	4.42
	4.43
	5.12
	5.57
	6.29
	7.34
	7.46
	7.57
	8.17
	8.36
	9.49
	10.1
	10.30
	12.18
	12.37
	13.43
	13.58
	14.18
	14.23
	14.44
	15.12
	16.4
	16.32
	16.54
	16.58
	17.35
	17.44
	18.20
	18.40
	19.8
	19.11
	19.14
	20.2
	20.19
	20.26
	21.25
	22.7
	22.28
	22.41
	23.22
	23.48
	23.50
	24.37
	25.8
	25.45
	26.4
	27.11
	27.38
	29.59
	29.38
	29.45
	29.58
	30.12
	30.28
	30.29
	30.59
	30.51
	31.3
	31.14
	31.40
	32.7
	32.14
	32.56
	33.2
	34.0
	35.5
	35.10
	35.56
	36.53
	37.16
	37.33
	40.53
	41.34
	42.23
	42.27
	42.33
	43.24
	43.54
	44.2
	44.52
	46.26
	47.34
	47.45
	48.10
	48.13
	48.25
	49.43
	50.12
	50.13
	50.32
	51.28
	51.38
	52.14
	53.10
	53.34
	54.7
	54.22
	54.48
	55.33
	55.35
	55.42
	55.57
	56.5
	56.17
	56.57
	57.35
	57.46
	57.53
	57.55
	57.59
	58.21
	59.11
	59.46
* ***edges***
/;

parameters
	B /
* ***attack_budget***
1
* ***attack_budget***
/
	D /
* ***target_degradation***
1
* ***target_degradation***
/
	weight(u_V)
	/
* ***node_weights***
		0 0.44
		1 0.95
		2 0.45
		3 0.24
		4 0.31
		5 0.28
		6 0.03
		7 0.39
		8 0.35
		9 0.38
		10 0.69
		11 0.06
		12 0.15
		13 0.48
		14 0.07
		15 0.37
		16 0.29
		17 0.34
		18 0.83
		19 0.37
		20 0.08
		21 0.71
		22 0.17
		23 0.75
		24 0.55
		25 0.55
		26 0.06
		27 0.87
		28 0.83
		29 0.56
		30 0.52
		31 0.96
		32 0.84
		33 0.21
		34 0.61
		35 1.0
		36 0.41
		37 0.97
		38 0.74
		39 0.75
		40 0.67
		41 0.4
		42 0.4
		43 0.3
		44 0.54
		45 0.06
		46 0.87
		47 0.79
		48 0.61
		49 0.25
		50 0.13
		51 0.29
		52 0.48
		53 0.62
		54 0.09
		55 0.48
		56 0.96
		57 0.95
		58 0.5
		59 0.96
* ***node_weights***
	/;

* ***input_data***
binary variables
    alpha(u_V) attacked
    beta(u_V) enabled
    y(u_V) supply is on
    ;

variables
    total_disabled_nodes
    attack_cost
    ;

equations
    objective_function_a
    objective_function_b
    e11(u_V)
    e12(u_V)
    e2(u_V)
    e3(u_V)
    e4(u_V)
    e5(u_V)
    e6_a
    e6_b
    ;

objective_function_a .. total_disabled_nodes =e=
    sum(u_V, 1 - beta(u_V));

objective_function_b .. attack_cost =e=
    sum(u_V, weight(u_V) * alpha(u_V));


$macro deg_in(u_V) sum(v_V $ e_E(v_V, u_V), 1)

e11(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= 1 - alpha(u_V);
e12(u_V) $ (deg_in(u_V) = 0) .. beta(u_V) =e= 1 - alpha(u_V);
e2(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= y(u_V);
e3(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =g= y(u_V) - alpha(u_V);
e4(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =g= 0;
e5(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =l= deg_in(u_V) - 1;
e6_a .. sum(u_V, weight(u_V) * alpha(u_V)) =l= B;
e6_b .. sum(u_V, 1 - beta(u_V)) / sum(u_V, 1) =g= D;


model damage_maximizer
    /
    objective_function_a,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_a
    /;

model attackCost_minimizer
    /
    objective_function_b,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_b
    /;

* solve damage_maximizer using mip maximizing total_disabled_nodes;
solve attackCost_minimizer using mip minimizing attack_cost;
