* model_15.gms

* ***input_data***
set u_V vertices in the graph
/ 0 * 179 /;

alias(u_V, v_V);
* ***node_count***
* 180
* ***node_count***
set e_E(u_V, v_V)
/
* ***edges***
	0.114
	0.141
	1.29
	1.142
	2.118
	3.53
	3.127
	4.20
	4.55
	5.53
	5.154
	6.58
	7.36
	8.11
	9.26
	9.169
	12.11
	12.100
	12.141
	13.118
	13.149
	14.39
	14.86
	15.71
	15.86
	15.101
	16.19
	16.87
	17.8
	17.168
	18.114
	19.38
	19.55
	21.23
	21.96
	21.101
	21.128
	21.169
	22.84
	22.171
	23.53
	23.63
	23.151
	24.123
	25.52
	25.72
	26.62
	27.162
	28.0
	28.81
	28.172
	29.50
	29.99
	29.174
	30.58
	32.12
	33.43
	33.106
	33.116
	33.135
	33.140
	34.92
	34.170
	35.17
	36.35
	36.76
	36.109
	37.4
	37.133
	37.147
	38.27
	38.123
	38.139
	39.27
	40.0
	40.59
	41.3
	41.66
	41.125
	42.2
	42.21
	44.54
	44.81
	45.34
	45.39
	45.96
	45.97
	46.125
	47.178
	48.89
	49.81
	50.110
	50.152
	51.26
	51.89
	51.103
	51.108
	52.16
	52.65
	52.127
	52.137
	53.130
	53.137
	53.158
	54.0
	55.10
	55.106
	57.29
	58.64
	58.123
	59.45
	59.50
	60.31
	60.34
	60.103
	60.110
	60.112
	61.48
	62.6
	62.102
	62.145
	63.30
	63.179
	63.148
	64.6
	64.67
	64.99
	65.1
	66.145
	67.61
	68.72
	71.60
	71.169
	72.73
	72.152
	73.10
	73.46
	73.96
	74.114
	75.67
	75.128
	76.26
	76.60
	77.122
	78.8
	78.71
	78.85
	78.133
	79.133
	80.118
	81.174
	82.77
	82.87
	82.131
	82.132
	83.33
	83.155
	84.61
	84.161
	84.162
	85.156
	85.167
	86.55
	86.105
	87.22
	87.53
	87.64
	87.117
	87.168
	88.140
	89.16
	89.39
	89.41
	92.123
	92.152
	92.158
	93.50
	93.172
	94.24
	94.44
	94.77
	94.104
	94.107
	95.47
	95.83
	96.160
	96.178
	97.3
	98.55
	98.76
	98.90
	99.13
	99.44
	99.133
	99.171
	101.11
	101.85
	101.137
	102.67
	103.127
	104.179
	105.5
	106.23
	106.25
	106.60
	107.59
	107.93
	108.6
	109.122
	109.137
	109.146
	110.123
	110.173
	111.148
	113.33
	113.62
	113.86
	113.158
	114.3
	115.93
	115.108
	115.170
	117.148
	118.59
	118.62
	118.122
	118.132
	119.10
	119.52
	119.144
	120.162
	121.73
	122.138
	122.164
	123.31
	124.108
	124.116
	125.136
	126.46
	126.66
	126.75
	127.70
	127.95
	128.40
	128.49
	129.43
	129.77
	129.159
	130.22
	130.76
	130.133
	131.98
	132.6
	132.20
	132.44
	132.83
	133.29
	133.120
	133.143
	134.33
	134.53
	134.155
	135.139
	136.88
	136.175
	137.38
	137.53
	138.53
	138.65
	138.118
	138.164
	139.18
	139.42
	139.43
	139.47
	139.109
	139.171
	140.89
	141.44
	141.70
	141.168
	142.39
	142.51
	142.162
	143.20
	143.49
	143.84
	144.34
	144.85
	145.7
	145.105
	146.27
	146.101
	147.6
	147.74
	147.175
	150.119
	151.31
	151.167
	152.15
	152.87
	152.115
	153.6
	153.15
	153.175
	154.94
	155.108
	155.128
	155.130
	155.145
	156.114
	157.21
	157.30
	157.47
	157.110
	158.162
	159.68
	159.166
	161.30
	161.51
	161.56
	162.83
	163.160
	164.168
	165.121
	165.177
	167.80
	168.13
	168.34
	169.13
	169.49
	169.91
	169.157
	169.175
	170.69
	170.76
	170.113
	171.44
	171.67
	171.104
	171.105
	171.176
	172.2
	172.43
	172.114
	173.152
	173.153
	174.21
	174.66
	174.85
	176.26
	176.58
	176.60
	176.156
	177.11
	177.61
	177.91
	178.52
	178.170
	179.143
* ***edges***
/;

parameters
	B /
* ***attack_budget***
1
* ***attack_budget***
/
	D /
* ***target_degradation***
1
* ***target_degradation***
/
	weight(u_V)
	/
* ***node_weights***
		0 0.88
		1 0.65
		2 0.51
		3 0.26
		4 0.07
		5 0.97
		6 0.13
		7 0.48
		8 0.31
		9 0.46
		10 0.1
		11 0.39
		12 0.31
		13 0.16
		14 0.78
		15 0.28
		16 0.94
		17 0.61
		18 0.2
		19 0.49
		20 0.75
		21 0.11
		22 0.61
		23 0.23
		24 0.65
		25 0.97
		26 0.15
		27 0.77
		28 0.26
		29 0.95
		30 0.76
		31 0.09
		32 0.89
		33 0.34
		34 0.38
		35 0.44
		36 0.02
		37 0.22
		38 0.5
		39 0.35
		40 0.91
		41 0.04
		42 0.76
		43 0.35
		44 0.72
		45 0.32
		46 0.03
		47 0.54
		48 0.31
		49 0.59
		50 0.01
		51 0.84
		52 0.57
		53 0.79
		54 0.63
		55 0.13
		56 0.65
		57 0.39
		58 0.99
		59 0.87
		60 0.46
		61 0.84
		62 0.81
		63 0.75
		64 0.41
		65 0.91
		66 0.72
		67 0.03
		68 0.41
		69 0.8
		70 0.4
		71 0.54
		72 0.18
		73 0.49
		74 0.72
		75 1.0
		76 0.34
		77 0.21
		78 0.52
		79 0.29
		80 0.73
		81 0.14
		82 0.95
		83 0.59
		84 0.65
		85 0.31
		86 0.37
		87 0.72
		88 0.61
		89 0.09
		90 0.85
		91 0.56
		92 0.97
		93 0.21
		94 0.09
		95 0.53
		96 0.77
		97 0.69
		98 0.21
		99 0.42
		100 0.21
		101 0.8
		102 0.4
		103 0.44
		104 0.93
		105 0.03
		106 0.12
		107 0.28
		108 0.61
		109 0.15
		110 0.34
		111 0.39
		112 0.22
		113 0.86
		114 0.03
		115 0.57
		116 0.92
		117 0.66
		118 0.44
		119 0.19
		120 0.25
		121 0.28
		122 0.38
		123 0.08
		124 0.6
		125 0.9
		126 0.4
		127 0.72
		128 0.4
		129 0.96
		130 0.43
		131 0.15
		132 0.95
		133 0.18
		134 0.4
		135 0.5
		136 0.95
		137 0.55
		138 0.21
		139 0.5
		140 0.84
		141 0.0
		142 0.68
		143 0.62
		144 0.48
		145 0.65
		146 0.56
		147 0.17
		148 0.02
		149 0.32
		150 0.21
		151 0.29
		152 0.76
		153 0.8
		154 0.88
		155 0.43
		156 0.8
		157 0.13
		158 0.57
		159 0.48
		160 0.33
		161 0.96
		162 0.22
		163 0.73
		164 0.55
		165 0.07
		166 0.19
		167 0.21
		168 0.0
		169 0.87
		170 0.63
		171 0.82
		172 0.57
		173 0.47
		174 0.08
		175 0.39
		176 0.32
		177 0.71
		178 0.91
		179 0.51
* ***node_weights***
	/;

* ***input_data***
binary variables
    alpha(u_V) attacked
    beta(u_V) enabled
    y(u_V) supply is on
    ;

variables
    total_disabled_nodes
    attack_cost
    ;

equations
    objective_function_a
    objective_function_b
    e11(u_V)
    e12(u_V)
    e2(u_V)
    e3(u_V)
    e4(u_V)
    e5(u_V)
    e6_a
    e6_b
    ;

objective_function_a .. total_disabled_nodes =e=
    sum(u_V, 1 - beta(u_V));

objective_function_b .. attack_cost =e=
    sum(u_V, weight(u_V) * alpha(u_V));


$macro deg_in(u_V) sum(v_V $ e_E(v_V, u_V), 1)

e11(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= 1 - alpha(u_V);
e12(u_V) $ (deg_in(u_V) = 0) .. beta(u_V) =e= 1 - alpha(u_V);
e2(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =l= y(u_V);
e3(u_V) $ (deg_in(u_V) > 0) .. beta(u_V) =g= y(u_V) - alpha(u_V);
e4(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =g= 0;
e5(u_V) $ (deg_in(u_V) > 0) .. deg_in(u_V) * y(u_V) - sum(v_V $ e_E(v_V, u_V), beta(v_V)) =l= deg_in(u_V) - 1;
e6_a .. sum(u_V, weight(u_V) * alpha(u_V)) =l= B;
e6_b .. sum(u_V, 1 - beta(u_V)) / sum(u_V, 1) =g= D;


model damage_maximizer
    /
    objective_function_a,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_a
    /;

model attackCost_minimizer
    /
    objective_function_b,
    e11,
    e12,
    e2,
    e3,
    e4,
    e5,
    e6_b
    /;

* solve damage_maximizer using mip maximizing total_disabled_nodes;
solve attackCost_minimizer using mip minimizing attack_cost;
