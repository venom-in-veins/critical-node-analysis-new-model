from GraphGenerator import GraphGenerator
from GamsCodeGenerator import GamsCodeGenerator
from TestFilesGenerator import TestFilesGenerator
from ResultFilesAnalyzer import ResultFilesAnalyzer
from SolvedProblemInstance import SolvedProblemInstance
from common import (
    draw,
)
from GamsInputReader import GamsInputReader

import os
dir_path = os.path.dirname(os.path.realpath(__file__))
org_model_path = dir_path+'/gams/org/model.gms'

def make_gams_file(graph):
    gams_file_generator = GamsCodeGenerator(
        org_model_path,
        graph,
    )

    gams_file_generator.make_gams_file(
        dir_path+'/gams/data/model2.gms',
    )

def display_solved_test(filename):
    target_folder = os.path.join(dir_path, 'gams/safe/uniform/barabasi/')
    problem_instance = SolvedProblemInstance.create(
        target_folder,
        filename,
    )
    draw(problem_instance.original_graph)
    draw(problem_instance.solution_graph)


def display_test(filename):
    target_folder = os.path.join(dir_path, 'gams/data/')
    reader = GamsInputReader(os.path.join(target_folder, f'{filename}.gms'))
    draw(reader.graph)

def run_analysis():
    analyzer = ResultFilesAnalyzer(
        f'{dir_path}/gams/data/',
        f'info.json',
    )
    # analyzer.draw()
    analyzer.run()
    analyzer.save()

def main():
    # tests_generator = TestFilesGenerator(
    #     dir_path+'/gams/org/model.gms',
    #     dir_path+'/gams/data/'
    # )
    # tests_generator.run()
    run_analysis()

    # graph = GraphGenerator.SmallerExample()
    # draw(graph)
    # make_gams_file(graph)

    # for i in range(1, 3):
    #     filename = f'model_{i}'
    #     display_solved_test(filename)
    # pass


main()
