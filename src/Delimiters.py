class Delimiters:
    org = f'* ***org***'
    input_data = f'* ***input_data***'
    node_weights = f'* ***node_weights***'
    node_count = f'* ***node_count***'
    edges = f'* ***edges***'
    attack_budget = f'* ***attack_budget***'
    target_degradation = f'* ***target_degradation***'