from GraphGenerator import GraphGenerator
from GamsCodeGenerator import GamsCodeGenerator

class TestFilesGenerator:
    def __init__(self, org_model_path, target_dir):
        self.org_model_path = org_model_path
        self.target_dir = target_dir
        self.number_of_files_for_each_node_count = 5
        self.min_edge_ratio = 10
        self.node_counts = [60, 120, 180, 240, 300]
        self.info = []

    def write_test_file(self, filename, number_of_nodes, number_of_edges_per_node):
        self.info.append({
            'filename': filename.split('.')[0],
            'number_of_nodes': number_of_nodes,
            'average_edges_per_node': number_of_edges_per_node,
        })

        graph = GraphGenerator.generate_random_barabasi(
            number_of_nodes=number_of_nodes,
            number_of_edges_per_node=number_of_edges_per_node,
        )
        gams_file_generator = GamsCodeGenerator(
            self.org_model_path,
            graph,
        )
        gams_file_generator.make_gams_file(
            self.target_dir + filename,
        )

    def write_info_file(self):
        import json
        with open(self.target_dir + 'info.json', 'w') as file:
            file.write(
                json.dumps(self.info)
            )

    def run(self):
        for i, number_of_nodes in enumerate(self.node_counts):
            edge_ratio_factor = int(self.min_edge_ratio / self.number_of_files_for_each_node_count)
            for file_number_for_node_count in range(1, self.number_of_files_for_each_node_count + 1):
                file_number = i * self.number_of_files_for_each_node_count + file_number_for_node_count
                number_of_edges_per_node = int(number_of_nodes * edge_ratio_factor * file_number_for_node_count / self.min_edge_ratio)
                filename = f'model_{file_number}.gms'
                self.write_test_file(filename, number_of_nodes, number_of_edges_per_node)
        self.write_info_file()



    