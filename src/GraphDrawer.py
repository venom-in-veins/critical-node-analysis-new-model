import igraph as iGraph

class GraphDrawer:
    def __init__(
            self,
            graph
    ):
        self.graph = graph.copy()

    def draw(self):
        lib_colors =\
            iGraph.drawing.colors
        self.add_color_for_vertices_org()
        self.add_color_for_edges_org()
        self.labels_for_vertices_org()
        iGraph.plot(
            self.graph,
            vertex_label_dist=0,
            background=lib_colors.color_name_to_rgb('white'),
            # bbox = (300, 300),
            # bbox = (720, 720),
            bbox = (1980, 1080),
        )

    def add_color_for_vertices_org(self):
        lib_colors =\
            iGraph.drawing.colors
        colors = []
        for v in self.graph.vs:
            if v['isEnabled']:
                colors.append(lib_colors.color_name_to_rgb('dark green'))
            elif v['isAttacked']:
                colors.append(lib_colors.color_name_to_rgb('yellow'))
            else:
                colors.append(lib_colors.color_name_to_rgb('red'))
        self.graph.vs['color'] = colors

    def labels_for_vertices_org(self):
        lib_colors =\
            iGraph.drawing.colors
        self.graph.vs['label'] = list(
            map(
                lambda v: GraphDrawer.label_for_vertex(v),
                self.graph.vs
            )
        )

        self.graph.vs['label_color'] = [lib_colors.color_name_to_rgb('black') for _ in self.graph.vs]
        self.graph.vs['label_size'] = [14 for _ in self.graph.vs]
        self.graph.vs['label_dist'] = [3 for _ in self.graph.vs]
        self.graph.vs['size'] = [20 for _ in self.graph.vs]

    @staticmethod
    def label_for_vertex(v):
        label = str(v.index) + ', ' + str(v['attack_cost'])
        key = f'[{v.index}]'
        return label

    def add_color_for_edges_org(self):
        lib_colors =\
            iGraph.drawing.colors
        colors = []
        for e in self.graph.es:
            colors.append(lib_colors.color_name_to_rgb('blue'))
        self.graph.es['color'] = colors
        self.graph.es['width'] = [1 for _ in self.graph.es]
        self.graph.es['arrow_size'] = [1 for _ in self.graph.es]

