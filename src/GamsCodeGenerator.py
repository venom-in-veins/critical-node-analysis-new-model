from Delimiters import Delimiters

class GamsCodeGenerator:
    def __init__(
        self,
        gams_original_code_file_path,
        graph,
        target_degradation=1,
        attack_budget=1,
    ):
        self.gams_original_code_file_path = gams_original_code_file_path
        self.graph = graph
        self.target_degradation = target_degradation
        self.attack_budget = attack_budget

    def get_code_from_gams_file(self):
        with open(self.gams_original_code_file_path) as file:
            file_data = file.read()
            parts = file_data.split(Delimiters.org)
            return parts[1].strip()

    @staticmethod
    def string_for_vertex(id):
        return f'{id}'

    def get_code_for_initializing_vertices(self):
        code =f'''\
set u_V vertices in the graph
/ {self.string_for_vertex(0)} * {self.string_for_vertex(len(self.graph.vs) - 1)} /;
'''
        return code

    def get_code_for_initializing_edges(self):
        code =f'''\
alias(u_V, v_V);
{Delimiters.node_count}
* {len(self.graph.vs)}
{Delimiters.node_count}
set e_E(u_V, v_V)
/
{Delimiters.edges}
'''
        for e in self.graph.es:
            code +=\
            f'\t{self.string_for_vertex(e.source)}.{self.string_for_vertex(e.target)}\n'
        code += f'{Delimiters.edges}\n'
        code += '/;\n'
        return code


    def get_code_for_initializing_weights(self):
        code = f'''\
parameters
\tB /
{Delimiters.attack_budget}
{self.attack_budget}
{Delimiters.attack_budget}
/
\tD /
{Delimiters.target_degradation}
{self.target_degradation}
{Delimiters.target_degradation}
/
\tweight(u_V)
\t/
{Delimiters.node_weights}
'''
        for v in self.graph.vs:
            node = self.string_for_vertex(v.index)
            attack_cost = v['attack_cost']
            code += f'\t\t{node} {attack_cost}\n'
        code += f'{Delimiters.node_weights}\n'
        code += '\t/;\n'
        return code

    def get_generated_gams_code(self):
        code_for_initializing_vertices = self.get_code_for_initializing_vertices()
        code_for_initializing_edges = self.get_code_for_initializing_edges()
        code_for_initializing_weights = self.get_code_for_initializing_weights()
        code = f'''
{Delimiters.input_data}
{code_for_initializing_vertices}
{code_for_initializing_edges}
{code_for_initializing_weights}
{Delimiters.input_data}
'''
        return code

    def generate_file(self, code, output_path):
        with open(output_path, 'w') as file:
            file.write(f'''* {output_path.split('/')[-1]}
{code}
''')
    
    def make_gams_file(self, output_path):
        import os
        dir_path = os.path.dirname(os.path.realpath(__file__))
        code_generated = self.get_generated_gams_code()
        code_from_file = self.get_code_from_gams_file()
        code = code_generated + code_from_file
        self.generate_file(code, output_path)

