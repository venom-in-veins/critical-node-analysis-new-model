from Delimiters import Delimiters
import igraph as iGraph

class GamsInputReader:
    def __init__(self, filepath):
        self.filepath = filepath
        self.readfile(filepath)
        self.generate_graph()

    def _extract_edges(self, input_data):
        return [
            tuple(
                map(
                    lambda x: int(x),
                    line.strip().split('.')
                )
            )
            for line in input_data
                        .split(Delimiters.edges)[1]
                        .strip()
                        .split('\n')
        ]

    def _extract_node_count(self, input_data):
        return int(input_data.split(Delimiters.node_count)[1].split(' ')[1])

    def _extract_node_weights(self, input_data):
        return [
            float(line.strip().split(' ')[1])
            for line in input_data
                        .split(Delimiters.node_weights)[1]
                        .strip()
                        .split('\n')
        ]

    def _extract_attack_budget(self, input_data):
        return int(input_data.split(Delimiters.attack_budget)[1])

    def _extract_target_degradation(self, input_data):
        return float(input_data.split(Delimiters.target_degradation)[1])

    def readfile(self, filepath):
        with open(filepath) as file:
            input_data = file.read().split(Delimiters.input_data)[1]

        self._node_count = self._extract_node_count(input_data)
        self._edges = self._extract_edges(input_data)
        self._node_weights = self._extract_node_weights(input_data)
        self.attack_budget = self._extract_attack_budget(input_data)
        self.target_degradation = self._extract_target_degradation(input_data)

    def generate_graph(self):
        graph = iGraph.Graph(directed=True)
        graph.add_vertices(self._node_count)
        for u, v in self._edges:
            graph.add_edge(u, v)
        for v in graph.vs:
            v['attack_cost'] = self._node_weights[v.index]
            v['isEnabled'] = True
        self.graph = graph