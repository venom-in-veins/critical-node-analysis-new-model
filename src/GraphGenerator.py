import igraph as iGraph
import random

class GraphGenerator:
    @staticmethod
    def set_original_properties_for(vertex):
        vertex['isAttacked'] = False
        vertex['isEnabled'] = True

    @staticmethod
    def generate_original_for(solution_graph):
        graph = solution_graph.copy()
        for v in graph.vs:
            GraphGenerator.set_original_properties_for(v)
        return graph

    @staticmethod
    def get_random_weight():
        weight = round(random.expovariate(2), 2)
        return weight


    @staticmethod
    def generate_random_barabasi(
        number_of_nodes=3,
        number_of_edges_per_node=1,
    ):
        graph = iGraph.Graph.Barabasi(
                    number_of_nodes,
                    number_of_edges_per_node,
                    directed=True
                )
        for index, v in enumerate(graph.vs):
            GraphGenerator.set_original_properties_for(v)
            v['attack_cost'] = GraphGenerator.get_random_weight()
        return graph

    @staticmethod
    def generate_random_erdos(
        number_of_nodes=3,
        number_of_edges_per_node=1,
    ):
        num_edges = min(number_of_nodes * number_of_edges_per_node, 2 * number_of_nodes)
        graph = iGraph.Graph.Erdos_Renyi(n=number_of_nodes, m=num_edges, directed=True)
        for index, v in enumerate(graph.vs):
            GraphGenerator.set_original_properties_for(v)
            v['attack_cost'] = GraphGenerator.get_random_weight()
        return graph

    @staticmethod
    def generate_from_neighbors(neighbors):
        g = iGraph.Graph(directed=True)
        g.add_vertices(len(neighbors))
        for v, ns in enumerate(neighbors):
            g.vs[v]['attack_cost'] = 1+(v % 5)
            GraphGenerator.set_original_properties_for(v)
            for n in ns:
                g.add_edge(v, n)

        return g

    @staticmethod
    def SmallExample():
        neighbors = [
            # 0
            [1, 2, 3, 7, 9, 12, 20],

            # 1
            [10],

            # 2
            [3, 4, 5],

            # 3
            [7],

            # 4
            [5, 8],

            # 5
            [6, 9],

            # 6
            [13, 21],

            # 7
            [23],

            # 8
            [18],

            # 9
            [14, 29],

            # 10
            [11, 15, 29],

            # 11
            [12, 13, 16, 17],

            # 12
            [13, 14, 20],

            # 13
            [15, 22],

            # 14
            [17],

            # 15
            [19],

            # 16
            [18, 19, 21],

            # 17
            [28],

            # 18
            [23],

            # 19
            [],

            # 20
            [21, 22, 23, 25],

            # 21
            [24],

            # 22
            [23, 25, 29],

            # 23
            [26],

            # 24
            [27],

            # 25
            [],

            # 26
            [27],

            # 27
            [28],

            # 28
            [29],

            # 29
            [],
        ]
        return GraphGenerator.generate_from_neighbors(neighbors)

    @staticmethod
    def SmallerExample():
        neighbors = [
            # 0
            [1, 2, 3, 7, 9],

            # 1
            [],

            # 2
            [3, 4, 5],

            # 3
            [7],

            # 4
            [5, 8],

            # 5
            [6, 9],

            # 6
            [],

            # 7
            [],

            # 8
            [],

            # 9
            [],
        ]
        return GraphGenerator.generate_from_neighbors(neighbors)

    @staticmethod
    def SmallestExample():
        neighbors = [
            # 0
            [2],

            # 1
            [2],

            # 2
            [],
        ]
        return GraphGenerator.generate_from_neighbors(neighbors)

