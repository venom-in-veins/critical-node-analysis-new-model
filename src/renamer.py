import os
import shutil

def rename_gams_results_in(target_dir):
	for dirname, dirnames, filenames in os.walk(target_dir):
		dirnames.sort()
		for i, directory in enumerate(dirnames):
			src = os.path.join(target_dir, directory, "out.gdx")
			new_name = f"model_{i+1}.gdx"
			dst = os.path.join(directory, new_name)
			os.rename(src)

def move_gams_results(from_dir, to_dir):
	for dirname, dirnames, filenames in os.walk(from_dir):
		dirnames.sort()
		for i, directory in enumerate(dirnames):
			filename = f"model_{i+1}.gdx"
			src = os.path.join(from_dir, directory, filename)
			dst = os.path.join(to_dir, filename)
			shutil.copy(src, dst)


move_gams_results('.', '/home/muhammadali/Documents/mv_dir')