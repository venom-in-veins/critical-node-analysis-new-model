import os, json
from common import (
    draw,
)

class ResultFileAnalyzer:
    def __init__(self, info, problem_instance):
        self.info = info
        self.problem_instance = problem_instance

    def draw(self):
        draw(self.problem_instance.original_graph)
        draw(self.problem_instance.solution_graph)

    def run(self):
        self._count_attacked_nodes()

    def _count_attacked_nodes(self):
        self.number_of_attacked_nodes = len(
            [
                v for v in self.problem_instance.solution_graph.vs 
                if v['isAttacked'] == True
            ]
        )
        self.info['number_of_attacked_nodes'] = self.number_of_attacked_nodes
