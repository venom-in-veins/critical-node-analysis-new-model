from GraphDrawer import GraphDrawer
import os

def solution_graph_cache_dir_path_for(target_folder):
    return os.path.join(target_folder, 'gdx_cache')


def read_info_file(target_folder, info_filename, problem_filename=None):
    with open(os.path.join(target_folder, info_filename)) as file:
        import json
        info = json.load(file)
    if problem_filename:
        index = next(i for i,v in enumerate(info) if v['filename'] == problem_filename)
        return index, info
    else:
        return info

def draw(graph):
    import random
    random.seed(123)
    d = GraphDrawer(graph)
    d.draw()

def solution_graph_filename_for(filename):
    return f'{filename}.solution.graph'

