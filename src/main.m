%% Cleanup
close all
clear
clc


%% Main
jsonData = jsondecode(fileread("gams/safe/normal/barabasi/new_info.json"));
generateGraphsForExecutionTimeVsNodeDegree(jsonData)

function generateGraphsForNumberOfAttackedNodesVsNodeDegree(jsonData)
    for i = 1 : 5 : 25
        xs = cell2mat({jsonData(i : i + 4).average_edges_per_node});
        ys = cell2mat({jsonData(i : i + 4).number_of_attacked_nodes});
        coefficients = polyfit(xs, ys, 1);
        xFit = linspace(min(xs), max(xs), 1000);
        yFit = polyval(coefficients , xFit);
        figure;
        set(gcf, 'Position', [100 100 300 150])
        xlabel("average node degree")
        ylabel("number of nodes attacked")
        title("total number of nodes = " + num2str(jsonData(i).number_of_nodes))
        hold on;
        plot(xs, ys, '*');
        plot(xFit, yFit, 'r-', 'LineWidth', 2);
        grid on;
    end
end

function generateGraphsForExecutionTimeVsNodeDegree(jsonData)
    for i = 1 : 5 : 25
        xs = cell2mat({jsonData(i : i + 4).average_edges_per_node});
        ys = cell2mat({jsonData(i : i + 4).execution_time});
        coefficients = polyfit(xs, ys, 1);
%         xFit = linspace(min(xs), max(xs), 1000);
%         yFit = polyval(coefficients , xFit);
        figure;
        set(gcf, 'Position', [100 100 300 150])
        xlabel("average node degree")
        ylabel("execution time (ticks)")
        title("total number of nodes = " + num2str(jsonData(i).number_of_nodes))
        hold on;
        plot(xs, ys, '*');
        plot(xs, ys, '-');
%         plot(xFit, yFit, 'r-', 'LineWidth', 2);
        grid on;
    end
end


